import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from 'firebase/app'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: () => import('../views/auth/Login.vue'),
      meta: {
        layout: 'Auth'
      }
    },
    {
      path: '/register',
      name: 'Register',
      component: () => import('../views/auth/Register.vue'),
      meta: {
        layout: 'Auth'
      }
    },
    {
      path: '/',
      name: 'Profile',
      component: () => import('../views/Profile.vue'),
      meta: {
        layout: 'Main',
        isNeedAuth: true
      }
    },
    {
      path: '/create-project',
      name: 'CreateProject',
      component: () => import('../views/project/Create.vue'),
      meta: {
        layout: 'Main',
        isNeedAuth: true
      }
    },
    {
      path: '/project/:id',
      name: 'Project',
      component: () => import('../views/project/Project.vue'),
      meta: {
        layout: 'Main',
        isNeedAuth: true
      },
      children: [
        {
          path: 'timer',
          name: 'Timer',
          component: () => import('../views/project/Timer.vue')
        }
      ]
    },
    {
      path: '/project/:id/table',
      name: 'Table',
      component: () => import('../views/project/Table.vue'),
      meta: {
        layout: 'Main',
        isNeedAuth: true
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  const isCurrentUser = firebase.auth().currentUser
  const isNeedAuthenticated = to.matched.some(record => record.meta.isNeedAuth)
  if (!isCurrentUser && isNeedAuthenticated) {
    next('/login')
  } else {
    next()
  }
})

export default router
