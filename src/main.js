import Vue from 'vue'
import App from './App.vue'
import Vuelidate from 'vuelidate'
import './registerServiceWorker'
import router from './router'
import store from './store'
import firebase from 'firebase/app'
import 'materialize-css/dist/js/materialize.min'
import dateFilter from './filters/date.filter'
import loader from './components/Loader'

import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false

Vue.use(Vuelidate)

Vue.filter('dateFormat', dateFilter)

Vue.component('loader', loader)

firebase.initializeApp({
  apiKey: 'AIzaSyB8svt03lKIxG304kOEQVaWxgidq4lCg_w',
  authDomain: 'time-watch-application.firebaseapp.com',
  databaseURL: 'https://time-watch-application.firebaseio.com',
  projectId: 'time-watch-application',
  storageBucket: 'time-watch-application.appspot.com',
  messagingSenderId: '164143137486',
  appId: '1:164143137486:web:f469a0030a1b4de238869f'
})

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
