import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import project from './modules/project'
import user from './modules/user'
import time from './modules/time'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: { auth, project, user, time }
})
