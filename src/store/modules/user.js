import firebase from 'firebase/app'

export default {
  state: {
    userName: ''
  },
  getters: {
    userName: s => s.userName
  },
  mutations: {
    setUserName (state, payload) {
      state.userName = payload
    },
    clearUserName (state) {
      state.userName = ''
    }
  },
  actions: {
    async getUserName ({ commit }) {
      const userName = firebase.auth().currentUser.toJSON().displayName
      commit('setUserName', userName)
    },
    async editProfile ({ dispatch, commit }, { name }) {
      try {
        const uid = await dispatch('getUserId')
        await firebase.database().ref(`/users/${uid}/`).update({ name })

        const user = firebase.auth().currentUser
        await user.updateProfile({
          displayName: name
        }).then(function () {
          console.log('Update display name successful.')
        }).catch(function () {
          console.log('An error happened in update display name.')
        })

        commit('setUserName', name)
      } catch (e) {
        console.log('Ошибка: ', e)
      }
    }
  }
}
