import firebase from 'firebase/app'

export default {
  state: {
    projects: {}
  },
  getters: {
    projects: s => s.projects
  },
  mutations: {
    setProjects (state, payload) {
      state.projects = payload
    },
    clearProjects (state) {
      state.projects = {}
    }
  },
  actions: {
    async createProject ({ dispatch }, { id, name, rate, hoursPerDay, dayCreate, monthCreate }) {
      try {
        const uid = await dispatch('getUserId')
        await firebase.database()
          .ref(`/users/${uid}/projects/${id}`)
          .set({ id, name, rate, hoursPerDay, dayCreate, monthCreate })
      } catch (e) {
        console.log('Ошибка: ', e)
      }
    },
    async getProjectById ({ dispatch, commit }, projectId) {
      try {
        const userId = await dispatch('getUserId')
        const project = await firebase.database().ref(`/users/${userId}/projects/${projectId}`)
          .once('value')
          .then(data => data.toJSON())
        commit('setProjects', project)
      } catch (e) {
        console.log(e)
      }
    },
    async getProjects ({ dispatch, commit }) {
      try {
        const userId = await dispatch('getUserId')
        const projects = await firebase.database().ref(`/users/${userId}/projects/`)
          .once('value')
          .then(data => data.toJSON())
        commit('setProjects', projects)
      } catch (e) {
        console.log(e)
      }
    },
    async deleteProject ({ commit, dispatch }, { id }) {
      try {
        const userId = await dispatch('getUserId')
        await firebase.database().ref(`users/${userId}/projects/${id}`).remove()
        await dispatch('getProjects')
      } catch (e) {
        console.log(e)
      }
    },
    async editProject ({ dispatch }, { id, name, rate, hoursPerDay }) {
      const userId = await dispatch('getUserId')
      try {
        await firebase.database().ref(`/users/${userId}/projects/${id}`).update({ name, rate, hoursPerDay })
        await dispatch('getProjects')
      } catch (e) {
        console.log('Ошибка: ', e)
      }
    }
  }
}
