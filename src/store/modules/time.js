/* eslint-disable dot-notation */
import firebase from 'firebase/app'

// noinspection ES6ShorthandObjectProperty
export default {
  state: {
    timeDataset: {}
  },
  getters: {
    timeDataset: state => state.timeDataset
  },
  mutations: {
    timeSet (state, timeData) {
      state.timeDataset = timeData
    },
    dayTimeSet (state, timeData) {
      state.timeDataset = timeData
    }
  },
  actions: {
    async newRecord ({ dispatch }, { time, projectId, recordId, nums }) {
      const userId = await dispatch('getUserId')
      try {
        await firebase.database()
          .ref(`/users/${userId}/projects/${projectId}/time/month${nums.month}/week${nums.week}/day${nums.day}/${recordId}`)
          .set(time)
      } catch (e) {
        console.log('Ошибка', e)
      }
    },
    async getTimeData ({ dispatch, commit }, projectId) {
      try {
        const userId = await dispatch('getUserId')
        const timeData = await firebase.database().ref(`/users/${userId}/projects/${projectId}/time/`)
          .once('value')
          .then(data => data.toJSON())
        commit('timeSet', timeData)
      } catch (e) {
        console.log(e)
      }
    },
    async getDayTimeData ({ dispatch, commit }, { projectId, nums }) {
      try {
        const userId = await dispatch('getUserId')
        const timeData = await firebase.database()
          .ref(`/users/${userId}/projects/${projectId}/time/month${nums.month}/week${nums.week}/day${nums.day}/`)
          .once('value')
          .then(data => data.toJSON())
        commit('dayTimeSet', timeData)
      } catch (e) {
        console.log(e)
      }
    }
  }
}
