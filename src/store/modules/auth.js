import firebase from 'firebase/app'

export default {
  actions: {
    async getUserId () {
      const userId = firebase.auth().currentUser
      return await userId ? userId.uid : null
    },
    async getLogin ({ commit }, { email, password }) {
      try {
        await firebase.auth().signInWithEmailAndPassword(email, password)
      } catch (e) {
        alert(e)
        throw e
      }
    },
    async register ({ dispatch, commit }, { id, email, password, name }) {
      try {
        await firebase.auth().createUserWithEmailAndPassword(email, password)

        const user = firebase.auth().currentUser
        const uid = await dispatch('getUserId')

        await user.updateProfile({
          displayName: name
        }).then(function () {
          console.log('Update display name successful.')
        }).catch(function () {
          console.log('An error happened in update display name.')
        })

        await firebase.database().ref(`/users/${uid}`).set({ id, name })
        commit('setCustomUserId', id)
      } catch (e) {
        alert(e)
        throw e
      }
    },
    async logout ({ commit }) {
      await firebase.auth().signOut()
      commit('clearUserName')
      commit('clearProjects')
    }
  }
}
