export default function dateFilter (value, format = 'date') {
  const options = {}

  if (format.includes('date')) { // 01 января 2020
    options.year = 'numeric'
    options.month = 'long'
    options.day = '2-digit'
  }
  if (format.includes('time')) { // 00:00:00
    options.hour = '2-digit'
    options.minute = '2-digit'
    options.second = '2-digit'
  }
  if (format.includes('DDMMYYYY')) { // 01.01.2020
    options.year = 'numeric'
    options.month = '2-digit'
    options.day = '2-digit'
  }
  if (format.includes('HHMM')) { // 00:00
    options.hour = '2-digit'
    options.minute = '2-digit'
  }
  if (format.includes('weekday')) { // пн
    options.weekday = 'short'
  }
  if (format.includes('year')) { // 2020
    options.year = 'numeric'
  }
  if (format.includes('month')) { // 01
    options.month = '2-digit'
  }
  if (format.includes('day')) { // 01
    options.day = '2-digit'
  }
  if (format.includes('hour')) { // 00
    options.hour = '2-digit'
  }
  if (format.includes('minute')) { // 00
    options.minute = '2-digit'
  }
  if (format.includes('second')) { // 00
    options.second = '2-digit'
  }

  return new Intl.DateTimeFormat('ru-RU', options).format(new Date(value))
}
